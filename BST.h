/*****************************************************************************
 * @file BST.h
 * @brief (Function prototype for various operations on singly link list)
 * 
 * These contains prototypes for various function of singly link list and 
 * eventually any macros, constsants, or global variables you need.
 * 
 * @author Akash Nair
 * @bug No known bugs.
 *****************************************************************************/

/*****************
 *Includes
 **************/
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<time.h>

/**************************
 *Defines
 **********************/

/***********************
 *Structures
 ************************/
struct BST_node
{
    int data;
    struct BST_node *right;
    struct BST_node *left;
};

/*********************************************************************
 *@brief (This function is to insert a new node in link list.) 
 *
 *@param int (integer is returned)
 *
 *@return (struct BST_node  is returned.)
 *******************************************************************/
struct BST_node* insert(struct BST_node *, int number);

/*********************************************************************
 *@brief (This function is used to create a new node in link list.) 
 *
 *@param int (integer is returned)
 *
 *@return (struct BST_node  is returned.)
 *******************************************************************/
struct BST_node *makenode(int);


bool search(struct BST_node*, int);


/***********************************************************************
 *@brief (This function can display the link list.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *
 *@return (void  is returned.)
 *********************************************************************/
void display(struct BST_node *); 


/***********************************************************************
 *@brief (This function displays the max number in link list.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *
 *@return (int is returned.)
 *********************************************************************/
int findmax(struct BST_node *); 

/***********************************************************************
 *@brief (This function displays the min number in link list.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *
 *@return (int  is returned.)
 *********************************************************************/
int findmin(struct BST_node *);

/***********************************************************************
 *@brief (This function displays the height of link list.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *
 *@return (int  is returned.)
 *********************************************************************/ 
int height(struct BST_node *); 

/***************************************************************************
 *@brief (This function can travese BST in level order.) 
 *
 *@param node* (pointer to node datatype.)
 *
 *@return (Integer value  is returned.)
 *********************************************************************/
float printlevel(struct BST_node *); 

/***************************************************************************
 *@brief (This function can travese BST in level order.) 
 *
 *@param node* (pointer to node datatype.)
 *
 *@return (Integer value  is returned.)
 *********************************************************************/
void printgivenlevel(struct BST_node *, int);

/***************************************************************************
 *@brief (This function can travese BST by post-order.) 
 *
 *@param node* (pointer to node datatype.)
 *
 *@return (Integer value  is returned.)
 *********************************************************************/
float post_order(struct BST_node *); 

/***************************************************************************
 *@brief (This function can travese BST by pre-order.) 
 *
 *@param node* (pointer to node datatype.)
 *
 *@return (Integer value  is returned.)
 *********************************************************************/
float pre_order(struct BST_node *); 

/***************************************************************************
 *@brief (This function can travese BST by in-order.) 
 *
 *@param node* (pointer to node datatype.)
 *
 *@return (Integer value  is returned.)
 *********************************************************************/
float inorder(struct BST_node *); 

/**********************************************************************
 *@brief (This function is used to list different conditions.) 
 *
 *@param (void)
 *
 *@return (int is returned.)
 *********************************************************************/
int list();

/**********************************************************************
 *@brief (This function is used to switch betwee different conditions.) 
 *
 *@param (void)
 *
 *@return (Void is returned.)
 *********************************************************************/
void switch1();


/***************************************************************************
 *@brief (This function can search user inserted data from BST.) 
 *
 *@param node* (pointer to node datatype.)
 *@param int  (it indicates user inserted search data.)
 *
 *@return (Integer value  is returned.)
 *********************************************************************/
void binarysearch_tree(struct BST_node *, int);


/***************************************************************************
 *@brief (This function can search user inserted data from BST(Method 2).) 
 *
 *@param node* (pointer to node datatype.)
 *@param int  (it indicates user inserted search data.)
 *
 *@return (Integer value  is returned.)
 *********************************************************************/
void binsearch_data(struct BST_node *);
