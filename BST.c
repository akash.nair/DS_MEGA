 /**************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file	 Binary_search.c
 * @brief:	 Develop a program to construct a binary search tree.
 *		 1) Tree data will be given by user, user enters EOF to express end of list.
 *		 2) Display level-order traversal to validate the sequence of integers entered by user.
 *		 3) Display BST tree in-order, pre-order and post-order traversal format.
 *		 4) Display timing and memory space information for each type of traversal
 *		 5) Input the same list in program 1 and perform search operation. Compared timing and space 
 *		    information to understand difference between different types of search.
 *
 *
 *
 * @Author       - Akash Nair
 * @bug		     - no bugs
 ***************************************************************************************************
 *
 * History
 *
 *                      
 * April/11/2018, Akash , Created (description)
 *
 **************************************************************************************************/
#include "BST.h"

int main()
{
    struct BST_node *root = NULL;
	int num, total, loop;

    int element;
	printf("TOTAL NODES IN TREE \n");
	scanf("%d", &total);
	
	for(loop = 0; loop < total; loop++) {
		printf("\nEnter number : ");
		scanf("%d", &num);
		if (root == NULL) {		
			root = insert(root, num);
		}
		else {
			insert(root, num);
		}
	}
	switch1(root);
    return 0;
}

int list()
{
    int choice;
    printf("\nENTER-1   TO  FIND HEIGHT OF TREE\n");
    printf("ENTER-2   TO FIND MINIMUM VALUE IN TREE\n");
    printf("ENTER-3   TO FIND MAXIMUM VALUE IN TREE\n");
    printf("ENTER-4   TO DISPLAY LEVEL TRAVERSAL\n");
    printf("ENTER-5   TO DISPLAY IN-ORDER TRAVERSAL\n");
    printf("ENTER-6   TO DISPLAY PRE-ORDER TRAVERSAL\n");
    printf("ENTER-7   TO DISPLAY POST ORDER TRAVERSAL\n");
    printf("ENTER-8   TO DISPLAY TREE\n");	
    printf("ENTER-9   TO SEARCH IF DATA IS THERE IN TREE OR NOT \n");
    printf("ENTER-10  TO EXIT\n\n");
    printf("ENTER YOUR CHOICE ");
    scanf("%d",&choice);
    return(choice);
}

void switch1(struct BST_node *root)
{
  while(1) {   
    switch(list()) {
      case 1:
         height(root);
		 printf("\n\t\tHEIGHT OF TREE IS : %d\n", height(root));
		break;
      case 2:
        findmin(root);
		printf("\n\t\tMIN NUMBER IN TREE : %d\n", findmin(root));
		break;    
      case 3:
	    findmax(root);
		printf("\n\t\tMAX NUMBER IN TREE : %d\n", findmax(root));
        break;
      case 4:
		printf("\n\t\tLEVEL TRAVERSAL\n");
		printf("\nTIME TAKEN FOR LEVEL TRAVERSING : %fseconds\n", printlevel(root));
        break;    
      case 5:
	   	printf("\n\t\tIN_ORDER TRAVERSAL\n");
		printf("\nTIME TAKEN FOR IN-ORDER TRAVERSING : %fseconds\n", inorder(root));
       	break;
      case 6:
	    printf("\n\t\tPRE_ORDER TRAVERSAL\n");
		printf("\nTIME TAKEN FOR PRE-ORDER TRAVERSING : %fseconds\n", pre_order(root));
        break;
      case 7:
        printf("\n\t\tPOST_ORDER TRAVERSAL\n");
		printf("\nTIME TAKEN FOR POST-ORDER TRAVERSING : %fseconds\n", post_order(root));
        break;
      case 8:
		display(root);
        break;
      case 9:
        binsearch_data(root);
        break;
      case 10:
        printf("Exit");
        exit(0);
        break;
      default:
        printf("\n\tInvalid choice\n");
        exit(0);
        break;
    }
  }
}

void display(struct BST_node *root)
{			
	if(root != NULL) {	
		display(root->left);
		printf("\t%d\n", root->data);
		display(root->right);
	}
}

struct BST_node* insert(struct BST_node *root, int number)
{
		if(root == NULL) {
			root = makenode(number);
		}

		else if(number < root->data) {
			root->left = insert(root->left, number);
		}

		else if(number > root->data) {
			root->right = insert(root->right, number);
		}
		return root;
}

struct BST_node *makenode(int number)
{
	struct BST_node *newnode = (struct BST_node *)malloc(sizeof(struct BST_node));
	newnode->data = number;
	newnode->left = NULL;
	newnode->right = NULL;
	
	return newnode;
}

/*Find MIN and MAX In BST */
int findmin(struct BST_node *root)
{
	if(root == NULL) {
		printf("TREE IS EMPTY\n");
		return -1;
	}
	else if(root->left == NULL) {
		return root->data;
	}
	return findmin(root->left);
}


int findmax(struct BST_node *root)
{
	if(root == NULL) {
		printf("TREE IS EMPTY\n");
		return -1;
	}
	else if(root->right == NULL) {
		return root->data;
	}
    return findmax(root->right);
}

/* Height of binary tree */
int height(struct BST_node *node)
{
	if (node == NULL) {
		return 0;
	}

	int left_height = height(node->left);
	int right_height = height(node->right);

	if(left_height > right_height)
		return(left_height+1);
	else
		return right_height+1;
}
/*  Level Order Traversal */
float printlevel(struct BST_node *root)
{
   	clock_t start;
   	start = clock();

	int height_tree, i;
	height_tree = height(root);

	for(i = 0; i<=height_tree; i++) {
		printgivenlevel(root, i);
	}

	start = clock() - start;
    	double time_taken = ((double)start)/CLOCKS_PER_SEC;
	return time_taken;
}

void printgivenlevel(struct BST_node *root, int level)
{
	if(root == NULL) {
		return ;
	}

	if(level == 1) {
		printf("\t%d\n", root->data);
	}
	else if (level > 1) {
		printgivenlevel(root->left, level - 1);
		printgivenlevel(root->right, level - 1);
	}
}

/*TREE in-order, pre-order, post-order traversal*/
float inorder(struct BST_node *root)
{
	clock_t start;
	start = clock();
	if(root == NULL) {
		return 0;
	}
	inorder(root->left);
	printf("\t%d\n", root->data);
	inorder(root->right);
	
	start = clock() - start;
	double time_taken = ((double)start)/CLOCKS_PER_SEC;
    return time_taken;
}

float pre_order(struct BST_node *root)
{
	clock_t start;
	start = clock();
	if(root == NULL) {
		return 0;
	}
	printf("\t%d\n", root->data);
	pre_order(root->left);
	pre_order(root->right);

    start = clock() - start;
    double time_taken = ((double)start)/CLOCKS_PER_SEC;
    return time_taken;
}

float post_order(struct BST_node *root)
{
    clock_t start;
    start = clock();
	if(root == NULL) {
		return 0;
	}
	post_order(root->left);
	post_order(root->right);
	printf("\t%d\n", root->data);

    start = clock() - start;
    double time_taken = ((double)start)/CLOCKS_PER_SEC;
    return time_taken;
}

void binarysearch_tree(struct BST_node *root, int element)
{
	if(root == NULL) {
		printf("\nNO DATA LIKE THIS IN TREE\n");
		return;	
	}
	else if (root->data == element) {
		printf("FOUND ELEMENT %d\n", root->data);
		return;
	}
	else if(root->data < element) {
		binarysearch_tree(root->right, element);
	}
	else if(root->data > element) {
		binarysearch_tree(root->left, element);
	}
    else{
        return;
    } 
}

void binsearch_data(struct BST_node *root)
{
    int element;
    printf("ENTER THE NUMBER TO BE SEARCHED\n");
    scanf("%d", &element);
    binarysearch_tree(root, element);
}


