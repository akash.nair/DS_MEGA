/*****************************************************************************
 * @file singly.h
 * @brief (Function prototype for various operations on singly link list)
 * 
 * These contains prototypes for various function of singly link list and 
 * eventually any macros, constsants, or global variables you need.
 * 
 * @author Akash Nair
 * @bug No known bugs.
 *****************************************************************************/

/*****************
 *Includes
 **************/
#include<stdio.h>
#include<stdlib.h>

/**************************
 *Defines
 **********************/

/***********************
 *Structures
 ************************/
struct node
{
    int data;
    struct node *next;
};

/***********************************************************************
 *@brief (This function can insert node at end of link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void is returned.
 **********************************************************************/
void append_at_end(struct node **);

/***********************************************************************
 *@brief (This function can delete node at beginning of link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void is returned.)
 **********************************************************************/
void push(struct node **);

/***********************************************************************
 *@brief (This function can display the link list.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *
 *@return (void  is returned.)
 *********************************************************************/
void display_list(struct node *); 

/*********************************************************************
 *@brief (This function can delete node at given position of link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void  is returned.)
 *********************************************************************/
void delete(struct node **);

/*********************************************************************
 *@brief (This function can delete entire link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void  is returned.)
 *********************************************************************/
void deletelinklist(struct node **);

/**********************************************************************
 *@brief (This function can delete node at given position of link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void  is returned.)
 *********************************************************************/
void deletepos(struct node **);

/**********************************************************************
 *@brief (This function is used to switch betwee different conditions.) 
 *
 *@param (void)
 *
 *@return (Void is returned.)
 *********************************************************************/
void switch1();

/**********************************************************************
 *@brief (This function is used to list different conditions.) 
 *
 *@param (void)
 *
 *@return (int is returned.)
 *********************************************************************/
int list();

/**********************************************************************
 *@brief (This function can insert node at given position of link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void  is returned.)
 *********************************************************************/
void insert_at_n(struct node **);

/*********************************************************************
 *@brief (This function can reverse the link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void  is returned.)
 *********************************************************************/
void reverse_linklist(struct node **);

/**********************************************************************
 *@brief (This function can reverse the link list using link list.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *
 *@return (Void is returned.)
 *********************************************************************/
void recursive(struct node *);

/**********************************************************************
 *@brief (This function can count the number of digits of input number.) 
 *
 *@param int (pointer to struct node.)
 *
 *@return (void is returned.)
 *********************************************************************/
void countnode(struct node *);
