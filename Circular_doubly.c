 /**************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file	 Circular_doubly.c
 * @brief:	 Write a C program which maintains doubly linked list and provides 
 *		 functionality of addition (at beginning, at end, at intermediate), 
 *		 deletion (at beginning, at end, at intermediate) and searching of nodes in  
 *		 this circular doubly linked list.
 *    
 *
 *
 * @Author       - Akash Nair
 * @bug		     - no bugs
 **************************************************************************************************
 *
 * History
 *
 *                      
 * April/06/2018, Akash , Created (description)
 *
 *************************************************************************************************/
#include "Circular_doubly.h"

int main()
{		
	struct node *head = NULL;
    switch1(head);  	
	return 0;
}

int list()
{
    int choice;
    printf("\n1   TO MAKE A CIRCULAR DOUBLY LINK LISt\n");
    printf("2   TO SEARCH A NUMBER IN DOUBLY CIRCULAR LINK LIST\n");
    printf("3   TOTAL NUMBER OF NODES\n");
    printf("4   TO DISPLAY LINK LIST\n");
    printf("5  EXIT \n\n");
    printf("Enter your choice ");
    scanf("%d",&choice);
    return(choice);
}



void switch1(struct node *head)
{
  while(1) {   
    switch(list()) {
      case 1:
        insert_at_end_circular(&head);
        break;
      case 2:
        search(&head);
		break;
      case 3:
        count(head);
        break;
      case 4:
       display(head);
       break;
      case 5:  
        printf("Exited\n");
        exit(0);
        break;
      default:
        printf("\n\tInvalid choice\n");
        exit(0);
        break;
    }
  }
}

void insert_at_end_circular(struct node **head)
{
	int number;
	struct node *element, *temp;
	element = (struct node *)malloc(sizeof(struct node));

    printf("enter element to insert\n");
    scanf("%d", &number);
	
	element->data = number;
	temp = *head;
	if(*head == NULL) {
		*head = element;
		element->prev = NULL;
		element->next = NULL;
		return;
	}
	if(temp->next == NULL && temp->prev == NULL) {
		temp->next = element;
		temp->prev = element;
		element->prev = temp;
		element->next = temp;
		return;
	}
	while(temp->next != *head) {
		temp = temp->next;
	}
	temp->next = element;
	element->prev = temp;
	element->next = *head;
	element->next->prev = element;
	return;
}

void display(struct node *head)
{
	struct node *temp = head;
	if(temp->next == NULL) {
		printf("\nCURRENT CIRCULAR HAs ONlY ONE NODE\n");
		printf("%d ", temp->data);
		printf("\n");
		return;
	}
	while(temp->next != head) {
		printf(" %d	", temp->data);
		temp = temp->next;
	}	
	printf("%d",temp->data);
	printf("\n\n");
}
/*
void search(struct node **ref_head) 
{	
	int search1;
    	struct node *temp = *ref_head;
    	printf("ENTER THE NUMBER TO SEARCH FROM NODE\n");
    	scanf("%d", &search1);	
		 do {
			if(temp->data == search1) {
				printf("data = %d\n", temp->data);
				return;
			}
			temp = temp->next;
		}while(temp != *ref_head);
		printf("NUMBER NOT FOUND IN LIST\n");
}
*/
void count(struct node *head) 
{	
	int count1 = 1;
	struct node *temp;
	temp = head;

	while(temp->next != head) {
		count1++;
		temp = temp->next;
	}
		printf("TOTAL NUMBER OF NODES IN CIRCULAR LINK LIST = %d", count1);
}

void search(struct node **ref_head) 
{   
    int search1, count1 = 1;
        struct node *temp = *ref_head;
        printf("ENTER THE NUMBER TO SEARCH FROM NODE\n");
        scanf("%d", &search1);  
         do {
            if(temp->data == search1) {
				printf("SEARCHING NUMBER FOUND AT POSITION %d WHICH IS %d", count1, temp->data);
                return;
            }
			count1++;
            temp = temp->next;
        }while(temp != *ref_head);
        printf("NUMBER NOT FOUND IN LIST\n");
}

