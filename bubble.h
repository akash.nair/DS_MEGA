/*****************************************************************************
 * @file bubble.h
 * @brief (Function prototype for various operations on singly link list)
 * 
 * These contains prototypes for various function of singly link list and 
 * eventually any macros, constsants, or global variables you need.
 * 
 * @author Akash Nair
 * @bug No known bugs.
 *****************************************************************************/


/*****************
 *Includes
 **************/
#include<stdio.h>
#include<stdlib.h>


/**************************
 *Defines
 **********************/
#define TRUE 1
#define FALSE 0

/***********************
 *Structures
 ************************/
struct node
{
    int data;
    struct node *next;
};

/***********************************************************************
 *@brief (This function can insert node at beginning of link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void is returned.)
 **********************************************************************/
void push(struct node **);


/***********************************************************************
 *@brief (This function can display the link list.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *
 *@return (void  is returned.)
 *********************************************************************/
void display_list(struct node *); 

/**********************************************************************
 *@brief (This function can count the number of digits of input number.) 
 *
 *@param int (pointer to struct node.)
 *
 *@return (void is returned.)
 *********************************************************************/
void countnode(struct node *); 


/*********************************************************************
 *@brief (This function can swap the data for given two address.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *@param struct node* (pointer to structure node datatype.)
 *
 *@return (Integer value  is returned.)
 ********************************************************************/
void swap(struct node *, struct node *); 


/**********************************************************************
 *@brief (This function can sort the link list using bubble sort algorithm.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *@param int  (it indicates the starting position)
 *@param int  (it indicates the ending position)
 *
 *@return (void  is returned.)
 *********************************************************************/
void bubble(struct node *);
