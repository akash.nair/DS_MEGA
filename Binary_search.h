
/*****************************************************************************
 * @file Binary_search.h
 * @brief (Function prototype for various operations on singly link list)
 * 
 * These contains prototypes for various function of singly link list and 
 * eventually any macros, constsants, or global variables you need.
 * 
 * @author Akash Nair
 * @bug No known bugs.
 *****************************************************************************/


/*****************
 *Includes
 **************/
#include<stdio.h>
#include<stdlib.h>


/**************************
 *Defines
 **********************/

/***********************
 *Structures
 ************************/
struct node
{
    int data;
    struct node *next;
};


/***********************************************************************
 *@brief (This function can insert node at end of link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void is returned.
 **********************************************************************/
void insert_at_end(struct node **);

/***********************************************************************
 *@brief (This function can display the link list.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *
 *@return (void  is returned.)
 *********************************************************************/
void display_list(struct node *); 


/**********************************************************************
 *@brief (This function can count the number of digits of input number.) 
 *
 *@param int (pointer to struct node.)
 *
 *@return (void is returned.)
 *********************************************************************/
int countnode(struct node *); 

/***************************************************************************
 *@brief (This function can search user inserted data from BST(METHOD: BINARY SEARCH).) 
 *
 *@param node* (pointer to node datatype.)
 *@param int  (it indicates user inserted search data.)
 *
 *@return (Integer value  is returned.)
 *********************************************************************/
void binary_search(struct node **, int, int);

/***************************************************************************
 *@brief (This function can search user inserted data from BST(METHOD: LINEAR SEARCH).) 
 *
 *@param node* (pointer to node datatype.)
 *@param int  (it indicates user inserted search data.)
 *
 *@return (Integer value  is returned.)
 *********************************************************************/
void linear_search(struct node *, int, int);
