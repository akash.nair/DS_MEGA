/*****************************************************************************
 * @file reverse_doubly.h
 * @brief (Function prototype for various operations on singly link list)
 * 
 * These contains prototypes for various function of singly link list and 
 * eventually any macros, constsants, or global variables you need.
 * 
 * @author Akash Nair
 * @bug No known bugs.
 *****************************************************************************/

/*****************
 *Includes
 **************/
#include<stdio.h>
#include<stdlib.h>

/**************************
 *Defines
 **********************/

/***********************
 *Structures
 ************************/
struct Doubly
{
    int data;
    struct Doubly *next;
    struct Doubly *prev;
};

/***************************************************************************
 *@brief (This function can insert node at end of link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void is returned.)
 **************************************************************************/
void insert(struct Doubly **, int);

/***********************************************************************
 *@brief (This function can display the link list.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *
 *@return (void  is returned.)
 *********************************************************************/
void display(struct Doubly *); 

/***********************************************************************
 *@brief (This function reverses the whole given doubly link list.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *
 *@return (struct pointer  is returned.)
 *********************************************************************/
struct Doubly * reverse(struct Doubly **);

