 /**************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file	 doubly.c
 * @brief:	 Write a C program which maintains doubly linked list and provides 
 *		 functionality of addition (at beginning, at end, at intermediate), 
 *		 deletion (at beginning, at end, at intermediate) and searching of nodes in  
 *		 this circular doubly linked list.
 *    
 *
 *
 * @Author       - Akash Nair
 * @bug		     - no bugs
 **************************************************************************************************
 *
 * History
 *
 *                      
 * April/06/2018, Akash , Created (description)
 *
 *************************************************************************************************/

#include "doubly.h"

int main()
{   
    struct node *head = NULL;   
    switch1(head);    
    return 0;
}


int list()
{
    int choice;
    printf("\n1   TO APPEND\n");
    printf("2   TO PUSH\n");
    printf("3   To INSERT AT Nth POSITION\n");
    printf("4   TO DISPLAY LINK LIST\n");
    printf("5   TO DELETE A NODE FRoM BeGGininG\n");
    printf("6   TO DELETE A NODE at end\n");
 	printf("7  EXIT \n\n");
    printf("Enter your choice ");
    scanf("%d",&choice);
    return(choice);
}


void switch1(struct node *head)
{
  while(1) {   
    switch(list()) {
      case 1:
        insert_at_end(&head);
        break;
      case 2:
        insert_at_begin(&head);
        break;
      case 3:
        insert_at_n(&head);
        break;    
      case 4:
       display(head);
       break;
      case 5:
		delete_at_begin(&head);
        break;    
      case 6:
		delete_at_end(&head);
        break;
      case 7:  
        printf("Exit\n");
        exit(0);
        break;
      default:
        printf("\n\tInvalid choice\n");
        exit(0);
        break;
    }
  }
}



void insert_at_end(struct node **ref_head)
{
    int number; 
    struct node *element, *temp;
    element = (struct node *)malloc(sizeof(struct node));

    printf("Enter number\n");
    scanf("%d", &number);

    element->data = number;

    if(*ref_head == NULL) {
        *ref_head = element;
        element->prev = NULL;
        element->next = NULL;
        return;
    }

    temp = *ref_head;
    while(temp->next != NULL) {
        temp = temp->next;
    }
    temp->next = element;
    element->prev = temp;
	element->next = NULL;
    return;
}



void display(struct node *head)
{
	if(head == NULL) {
		printf("LIST IS EMPTY\n");
	}
    while(head != NULL) {
    printf("CURRENT LIST IS %d  ", head->data);
    head = head->next;
    }
    printf("\n\n\n");
}

void delete_at_begin(struct node **ref_head)
{
		struct node *temp = *ref_head;
		*ref_head = temp->next;
		temp->prev = NULL;
		free(temp);
		printf("Deleted a node from beginning \n Display it by pressing 4\n");
}

void delete_at_end(struct node **ref_head)
{
	struct node *temp = *ref_head;
	if(temp->next != NULL) {
	while(temp->next != NULL) {
		temp = temp->next;
	}	
    temp->prev->next = NULL;
    printf("Deleted a node from beginning \n Display it by pressing 4\n");
	free(temp);
	}
	else {
        temp->data = 0;
		free(temp);
	}
}


void insert_at_n(struct node **ref_head)
{
    int number, position, loop, count;
    struct node *element, *temp;
    element = (struct node *)malloc(sizeof(struct node));
    
    printf("Enter the number and position to insert\n");
    printf("Enter number\n");
    scanf("%d", &number);
    printf("Enter position\n");
    scanf("%d", &position);

    element->data = number;
    element->next = NULL;
    
    if(position == 1) {
            element->prev = NULL;   
            element->next = *ref_head;
            *ref_head = element;
            return;    
    }   
    
    temp = *ref_head;
    
    for(loop = 0; loop < position - 1; loop++) {
        temp = temp->next;
    }   
        temp->prev->next = element;
        element->prev = temp->prev;
        element->next = temp;
        temp->prev = element;
        return;
}


void insert_at_begin(struct node **ref_head)
{
        int number;
        struct node *element, *first;
        element = (struct node *)malloc(sizeof(struct node));

        printf("Enter number to push in the list\n");
        scanf("%d", &number);

        element->data = number;

        if(*ref_head == NULL) {
            *ref_head = element;
            element->prev = NULL;
            element->next = NULL;
            return;
        }

        first = *ref_head;
        element->next = first;
        first->prev = element;
        element->prev = NULL;

        *ref_head = element;
}
/*
void insert_at_end_circular(struct node **head)
{
    int number;
    struct node *element, *temp;
    element = (struct node *)malloc(sizeof(struct node));

    printf("enter element to insert\n");
    scanf("%d", &number);
    
    element->data = number;
    temp = *head;
    if(*head == NULL) {
        *head = element;
        element->prev = NULL;
        element->next = NULL;
        return;
    }   
    if(temp->next == NULL && temp->prev == NULL) {
        temp->next = element;
        temp->prev = element;
        element->prev = temp;
        element->next = temp;
        return;
    }   
    while(temp->next != *head) {
        temp = temp->next;
    }   
    temp->next = element;
    element->prev = temp;
    element->next = *head;
    element->next->prev = element;
    return;
}
 
void search(struct node **ref_head)
{
    int search1, count1 = 1;
        struct node *temp = *ref_head;
        printf("ENTER THE NUMBER TO SEARCH FROM NODE\n");
        scanf("%d", &search1);
         do {
            if(temp->data == search1) {
                printf("SEARCHING NUMBER FOUND AT POSITION %d WHICH IS %d", count1, temp->data);
                return;
            }
            count1++;
            temp = temp->next;
        }while(temp != *ref_head);
        printf("NUMBER NOT FOUND IN LIST\n");
}


void count(struct node *head)
{
    int count1 = 1;
    struct node *temp;
    temp = head;

    while(temp->next != head) {
        count1++;
        temp = temp->next;
    }
        printf("TOTAL NUMBER OF NODES IN CIRCULAR LINK LIST = %d", count1);
}
*/
