/**************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file	 bubblesort.c
 * @brief:	Modify a C program of singly link list from day - 1 & 2 and 
 * 		add sorting methods into library: Bubble, Radix and Quick (using Linux library API).
 *    
 *
 *
 * @Author       - Akash Nair
 * @bug		     - no bugs
 **************************************************************************************************
 *
 * History
 *
 *                      
 * April/06/2018, Akash , Created (description)
 *
 *************************************************************************************************/
#include "bubble.h"

int main()
{
    int num, i;
    struct node *head = NULL;

    printf("ENTER TOTAL NUMBER TO ADD IN LIST\n");
    scanf("%d", &num);

    for(i = 0; i < num; i++) {
        push(&head);    
    }
	printf("LIST ENTERED BY YOU\n");   
    display_list(head);
	bubble(head);
	printf("SORTED LIST BY BUBBLE SORTING METHOD\n");
	display_list(head);
	return 0;
}

void push(struct node **ref_head)
{
    int number; 
    struct node *element;
    element = (struct node *)malloc(sizeof(struct node));

    printf("NUMBER ");
    scanf("%d", &number);

    element->data = number;
    element->next = *ref_head;

    *ref_head = element;
}

void display_list(struct node *temp)
{
    while(temp != NULL) {
        printf("\t%d", temp->data);
        temp = temp->next;
    }
    printf("\n");
}


void swap(struct node *add1, struct node *add2)
{
    int tempvar;
    tempvar = add1->data;
    add1->data = add2->data;
    add2->data = tempvar;
}


void bubble(struct node *head) 
{
	int swapped, i;
	struct node *temp = head;
	do {
		swapped = FALSE;
		temp = head;		
		while(temp->next != NULL) {
	    	if(temp->data > temp->next->data) {
            	swap(temp, temp->next);
            	swapped = 1; 	
			} 
		temp = temp->next;
		}
	  }while(swapped);
}
