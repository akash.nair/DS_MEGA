/*************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file	 Program1.c
 * @brief:	 A C program to generate singly link list based on user value.
 *			 1) Function inserts node first or last position. 
 *			 2) Displays linked list data values.
 *			 3) Insert node at specified node position
 *           4) Delete node of specified value irrespective of position.
 *			 5) Delete's the whole link list .
 *			 6) Reverse the list using Recursion.
 *			 7) Size of link list.
 * 		
 *    
 *
 *
 *
 * @Author       - Akash Nair
 * @bug		     - no bugs
 **************************************************************************************************
 *
 * History
 *
 *                      
 * April/02/2018, Akash , Created (description)
 *
 *************************************************************************************************/

#include "singly.h"

int main()
{
	struct node *head = NULL;
    int num, i;
	switch1(head);
	return 0;
}

void switch1(struct node *head)
{
  while(1) {   
    switch(list()) {
      case 1:
        append_at_end(&head);
        break;
      case 2:
        push(&head);
        break;
      case 3:
        delete(&head);
        break; 
      case 4:
        deletelinklist(&head);
		break;   
      case 5:
        insert_at_n(&head);
        break;     
      case 6:
       display_list(head);
       break;
      case 7:
        reverse_linklist(&head);
	  	break;
      case 8:
		recursive(head);
        break;
      case 9:
        countnode(head);
		break;
	  case 10:
	    printf("Exit");
        exit(0);
        break;
      default:
        printf("\n\tInvalid choice\n");
		exit(0);
        break;
    }   
  }
}

int list()
{
    int choice;
    printf("\n1   TO ADD ELEMENT AT END(QUEUE)\n");
    printf("2   TO PUSH AT BEGINNING(STACK)\n");
    printf("3   TO DELETE A VALUE\n");
    printf("4   TO DELETE WHOLE LINKED LIST\n");
    printf("5   To insert at nth pos\n");
	printf("6   TO DISPLAY LINK LIST\n");
	printf("7   TO REVERSE LINK lIST\n");
	printf("8   TO REVERSE LINK lIST USING RECURSION\n");
    printf("9   SIZE OF LINK LIST\n");
	printf("10  TO EXIT\n");
	scanf("%d",&choice);
    return(choice);
}

void delete(struct node **ref_head)
{
	int number;
	struct node *temp = *ref_head, *prev;
	
	printf("Enter the number to be deleted");
	scanf("%d", &number);

	if(temp != NULL && temp->data == number) {	
		*ref_head = temp->next;
		free(temp);
		return;
	}
	
	while(temp != NULL && temp->data != number) {
		prev = temp;
		temp = temp->next;
	}	

	prev->next = temp->next;
	free(temp);
}

void deletelinklist(struct node **ref_head)
{
	struct node *current = *ref_head;
	struct node *nextpos;

	while(current != NULL) {
		nextpos = current->next;
		free(current);
		current = nextpos;
	}
	*ref_head = NULL;
}

void append_at_end(struct node** ref_head)
{
	int number;
	struct node *element, *last;

	element = ( struct node *)malloc(sizeof( struct node));
	last = *ref_head;
	
	printf("Enter number \n");
	scanf("%d", &number);

	element->data = number;
	element->next = NULL;
	
	if (*ref_head == NULL) {
		*ref_head =element;
		return;
	}	
	while(last->next != NULL) {
		last = last->next;
	}
	last->next = element;
   	return; 
  
}

void push(struct node **ref_head)
{
	int number;
	struct node *element;
	element = (struct node *)malloc(sizeof(struct node));
	
	printf("Enter the number to be pushed\n");
	scanf("%d", &number);

	element->data = number;
	element->next = *ref_head;

	*ref_head = element;
}

void display_list(struct node *temp)
{
	while(temp != NULL) {
		printf("\tCURRENT LIST---%d---- ", temp->data);
		temp = temp->next;
	}
	printf("\n");
}

void insert_at_n(struct node **ref_head)
{
	 struct node *element;
	 element = (struct node *)malloc(sizeof(struct node));
	
    	 int num, pos, i;
   	 printf("Enter the number and position\n");
   	 scanf("%d %d", &num, &pos);

	element->data = num;
	
    	if(pos == 1) {
		element->next = *ref_head;
		*ref_head = element;
		return;	
	}
	struct node *element1 = *ref_head;
	
	for(i = 1; i < pos - 1; i++) {
		element1 = element1->next;	
	}
	element->next = element1->next;
	element1->next = element;
}

void reverse_linklist(struct node **ref_head)
{
	struct node *prev = NULL;
	struct node *current = *ref_head;
	struct node *next = NULL;
	
	while(current != NULL) {
		next = current->next;
		current->next = prev;
		prev = current;
		current = next;
	}
	*ref_head = prev;
}

void recursive(struct node *ref_head)
{
    if (ref_head == NULL) {
        return;
    }
    recursive(ref_head->next);
    printf("CURRENT LIST---%d ---", ref_head->data);
}


void countnode(struct node *head) 
{
    int count_node = 0;
    struct node *temp = head;
    if(temp == NULL) {
        count_node = 0;
        printf("SIZE OF LIST = %d",count_node);
    }
    else if(temp != NULL) {
        if(temp->next == NULL) {
            count_node = count_node + 1;
            printf("SIZE OF LIST  = %d", count_node);
        }
        while(temp->next != NULL) {
            count_node++;
            temp = temp->next;
        }
        printf("TOTAL SIZE OF LIST = %d\n", count_node+1);
    }   
}
