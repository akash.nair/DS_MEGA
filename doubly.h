/*****************************************************************************
 * @file doubly.h
 * @brief (Function prototype for various operations on singly link list)
 * 
 * These contains prototypes for various function of singly link list and 
 * eventually any macros, constsants, or global variables you need.
 * 
 * @author Akash Nair
 * @bug No known bugs.
 *****************************************************************************/

/*****************
 *Includes
 **************/
#include<stdio.h>
#include<stdlib.h>


/**************************
 *Defines
 **********************/

/***********************
 *Structures
 ************************/
struct node
{
    int data;
    struct node *next;
    struct node *prev;
};



/***********************************************************************
 *@brief (This function can insert node at end of link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void is returned.
 **********************************************************************/
void insert_at_end(struct node **);

/***********************************************************************
 *@brief (This function can display the link list.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *
 *@return (void  is returned.)
 *********************************************************************/
void display(struct node *); 

/**********************************************************************
 *@brief (This function is used to switch betwee different conditions.) 
 *
 *@param (void)
 *
 *@return (Void is returned.)
 *********************************************************************/
void switch1(struct node *); 

/**********************************************************************
 *@brief (This function is used to list different conditions.) 
 *
 *@param (void)
 *
 *@return (int is returned.)
 *********************************************************************/
int list();

/***************************************************************************
 *@brief (This function can delete node at beginning of link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void is returned.)
 *********************************************************************/
void delete_at_begin(struct node **);

/***************************************************************************
 *@brief (This function can delete node at end of link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void is returned.)
 *********************************************************************/
void delete_at_end(struct node **);

/***********************************************************************
 *@brief (This function can insert node at end of link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void is returned.
 **********************************************************************/
void insert_at_n(struct node **);

/***********************************************************************
 *@brief (This function can insert node at beginning of link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void is returned.)
 **********************************************************************/
void insert_at_begin(struct node **); 

/***********************************************************************
 *@brief (This function can insert node in circular way of a given link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void is returned.)
 **********************************************************************/
void insert_at_end_circular(struct node **);

/***********************************************************************
 *@brief (This function can search an element from given link list.) 
 *
 *@param struct node** (pointer to structure node datatype.)
 *
 *@return (void is returned.)
 **********************************************************************/
void search(struct node **);

/**********************************************************************
 *@brief (This function can count the number of digits of input number.) 
 *
 *@param int (pointer to struct node.)
 *
 *@return (void is returned.)
 *********************************************************************/
void count(struct node *); 
