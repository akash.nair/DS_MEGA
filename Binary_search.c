
 /**************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file	 Binary_search.c
 * @brief:	 A C program of singly link list from day - 1 & 2 and add search methods into library: 
 *		     linear search & binary search..
 *    
 *
 *
 * @Author       - Akash Nair
 * @bug		     - no bugs
 **************************************************************************************************
 *
 * History
 *
 *                      
 * April/09/2018, Akash , Created (description)
 *
 *************************************************************************************************/

#include "Binary_search.h"

int main()
{
    int flag, num, loop, search_element, total_node, search_number;
    struct node *head = NULL;

	printf("Enter total numbers\n");
    scanf("%d", &num);

    for(loop = 0; loop < num; loop++) {
        insert_at_end(&head);
    }

    display_list(head);

	printf("ENTER 1 FOR BINARY SEARCH\n");
	printf("ENTER 2 LINEAR SEARCH\n");

	printf("Enter your choice : ");
	scanf("%d", &flag);

    total_node =  countnode(head);

    if(flag == 1) {
		printf("Enter number to search \n");
  		scanf("%d", &search_element);
        binary_search(&head, total_node, search_element);    
		return 1;
    }
    else if (flag == 2){
		printf("Enter number to search \n");
        scanf("%d", &search_number);
        linear_search(head, total_node, search_number);
    } 
	else {
		printf("ERROR\n");
	}
    return 0;
}

void insert_at_end(struct node** ref_head)
{
    int number, append;
    struct node *element, *last;

    element = ( struct node *)malloc(sizeof( struct node));
    last = *ref_head;
    
    printf("Enter number \n");
    scanf("%d", &number);

    element->data = number;
    element->next = NULL;

    if (*ref_head == NULL) {
        *ref_head = element;
        return;
    }
    while(last->next != NULL) {
        last = last->next;
    }
    last->next = element;
    return;
}

void display_list(struct node *temp)
{
    while(temp != NULL) {
        printf("\t%d", temp->data);
        temp = temp->next;
    }
    printf("\n");
}

int countnode(struct node *head) 
{
    int count_node = 0;
    struct node *temp = head;
    if(temp == NULL) {
        count_node = 0;
        printf("number of nodes = %d",count_node);
        return count_node;
    }   

    else if(temp != NULL) {
        if(temp->next == NULL) {
            count_node = count_node + 1;
            printf("number of nodes = %d", count_node);
            return count_node;
        }
        while(temp->next != NULL) {
            count_node++;
            temp = temp->next;
        }
        printf("number of node = %d\n", count_node+1);
        return count_node+1;
    }   
}


void binary_search(struct node **ref_head, int total_node, int search_element)
{			
	int max_node, mid_index, loop;
	struct node *temp;
	struct node *head = *ref_head;
	
	max_node = total_node;

	mid_index = (max_node / 2);
	
	for(loop = 1; loop < mid_index; loop++) {
		head = head->next;
	}

	temp = head;

	if(temp->data == search_element) {
		printf("Data found %d\n", temp->data);
		return;
	}
	if(temp->data > search_element) {
		binary_search(ref_head, mid_index, search_element);
	}
	else if(head->data < search_element) {
		 binary_search(&temp->next, max_node+1, search_element);
	}
}

void linear_search(struct node *head, int total_node, int search_number)
{
	while(head->next != NULL) {
		if(head->data == search_number) {
			printf("Data found %d\n", head->data);
			return;
		}

		head = head->next;

		if(head->data == search_number){
			printf("Data found %d\n", head->data);
			return;
		}
	}
	printf("NO SUCH NUMBER IN LIST\n");
}
