/******************************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file singly_linklist.c
 * @brief (A C program in doubly linked list to reverse whole list and return last node as address node.
 *
 * @Author     -    Akash Nair
 * @bug        -    no known bugs
 *******************************************************************************************************
 * History
 *     
 * April/15/2018, Akash ,  Nothing Added.
 *     
 *******************************************************************************************************/
#include "reverse_doubly.h"

int main()
{
	struct Doubly *head = NULL;
	
	insert(&head, 13);
	insert(&head, 25);
	insert(&head, 45);
	insert(&head, 78);
	insert(&head, 100);
   	insert(&head, 1780);
    	insert(&head, 890);
    	insert(&head, 560);
	insert(&head, 500);

	display(head);

	reverse(&head);

	display(head);
	return 0;
}

void display(struct Doubly *head)
{
    if(head == NULL) {
        printf("LIST IS EMPTY\n");
    }   
    while(head != NULL) {
    printf(" %d  ", head->data);
    head = head->next;
    }   
    printf("\n\n\n");
}

void insert(struct Doubly **ref_head, int number)
{
    struct Doubly *element, *temp;
    element = (struct Doubly *)malloc(sizeof(struct Doubly));

    element->data = number;

    if(*ref_head == NULL) {
        *ref_head = element;
        element->prev = NULL;
        element->next = NULL;
        return;
    }

    temp = *ref_head;
    while(temp->next != NULL) {
        temp = temp->next;
    }
    temp->next = element;
    element->prev = temp;
	element->next = NULL;
	
	return;
}    

struct Doubly* reverse(struct Doubly **ref_head)
{
	struct Doubly *temp = *ref_head;
	struct Doubly *next;

	while(temp->next != NULL) {
		temp = temp->next;
	}

	*ref_head = temp;

	if(temp->next == NULL) {
		temp->next = temp->prev;
		temp->prev = NULL;
		temp = temp->next;
	}
	while(temp->prev != NULL) {
		next = temp->next;
		temp->next = temp->prev;
		temp->prev = next;
		temp = temp->next;
	}
	if(temp->prev == NULL) {
		temp->prev = temp->next;
		temp->next = NULL;
	}	
	return *ref_head;
}

