/************************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:
 * @file singly_linklist.c
 * @brief (A C program in non - linear linked list to check if tree is BST or not.
 *
 * @Author     - 	Akash Nair
 * @bug        - 	no known bugs
 **********************************************************************************
 * History
 *     
 * April/15/2018, Akash , Check if Binary Search tree or not.
 *     
 ************************************************************************************/
#include "BST_or_not.h"

int main()
{
	struct BST_node *root = NULL;
	int BST;

	root = makenode(20);
	root->left = makenode(10);
	root->right = makenode(30);
	root->left->left = makenode(8);
	root->right->right = makenode(35);
	
	BST = isbinary(root);

	if(BST == 1) {
		printf("GIVEN TREE IS A BINARY SEARCH TREE\n");
	}
	else {
		printf("NOT A BINARY SEARCH TREE\n");
	}

	display(root);
	return 0;
}

struct BST_node *makenode(int number)
{
    struct BST_node *newnode = (struct BST_node *)malloc(sizeof(struct BST_node));

    newnode->data = number;
    newnode->left = NULL;
    newnode->right = NULL;
    
    return newnode;
}

void display(struct BST_node *root)
{
    if(root != NULL) {
        display(root->left);
        printf("\t%d\n", root->data);
        display(root->right);
    }
}

int isbinary(struct BST_node *root)
{
	if(root == NULL) {
		return 1;
	}
	if(left_sub(root->left, root->data) && right_sub(root->right, root->data) &&
	   isbinary(root->left) && isbinary(root->right)) {
		return 1;
	}
	else {
		return 0;
	}
}

int left_sub(struct BST_node *root, int value) 
{
		if(root == NULL) {
			return 1;
		}
		if(root->data <= value && left_sub(root->left, value) && left_sub(root->right, value)) {
			return 1;
		}
		else {
			return 0;
		}
}

int right_sub(struct BST_node *root, int value) 
{
        if(root == NULL) {
            return 1;
        }
        if(root->data >= value && right_sub(root->right, value) && left_sub(root->left, value)) {
            return 1;
		}
        else {
            return 0;
		}
}


