/*****************************************************************************
 * @file BST_or_not.h
 * @brief (Function prototype for various operations on singly link list)
 * 
 * These contains prototypes for various function of singly link list and 
 * eventually any macros, constsants, or global variables you need.
 * 
 * @author Akash Nair
 * @bug No known bugs.
 *****************************************************************************/

/*****************
 *Includes
 **************/
#include<stdio.h>
#include<stdlib.h>

/**************************
 *Defines
 **********************/

/***********************
 *Structures
 ************************/
struct BST_node
{
    int data;
    struct BST_node *right;
    struct BST_node *left;
};


/*********************************************************************
 *@brief (This function is used to create a new node in link list.) 
 *
 *@param int (integer is returned)
 *
 *@return (struct BST_node  is returned.)
 *******************************************************************/
struct BST_node *makenode(int);

/********************************************************************
 *@brief (This function can display the link list.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *
 *@return (void  is returned.)
 *******************************************************************/
void display(struct BST_node *);

/********************************************************************
 *@brief (This function checks whether link list is BST or not.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *
 *@return (int is returned.)
 *******************************************************************/
int isbinary(struct BST_node *);

/*********************************************************************
 *@brief (This function checks the left side of link list.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *@param int (integer)

 *@return (int is returned.)
 ********************************************************************/
int left_sub(struct BST_node *, int);

/*********************************************************************
 *@brief (This function checks the right side of link list.) 
 *
 *@param struct node* (pointer to structure node datatype.)
 *@param int (integer)

 *@return (int is returned.)
 *********************************************************************/
int right_sub(struct BST_node *, int);


